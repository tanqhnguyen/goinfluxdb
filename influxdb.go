package goinfluxdb

import (
	"github.com/tanqhnguyen/envconfig"

	influxdb2 "github.com/influxdata/influxdb-client-go"
)

// Config is the configuration to connect to an influxdb
type Config struct {
	Token string `envconfig:"token" file_content:"true"`
	URL   string `envconfig:"url" required:"true"`
}

const defaultPrefix = "INFLUXDB"

// NewInfluxDBConfigFromEnvironment returns a new config based on env variables
// prefix is without "_". For example INFLUXDB will match
// INFLUXDB_DATABASE, INFLUXDB_USERNAME, INFLUXDB_PASSWORD, INFLUXDB_TOKEN, INFLUXDB_HOST, INFLUXDB_PORT
func NewInfluxDBConfigFromEnvironment(prefix string) (*Config, error) {
	var fromEnv Config
	err := envconfig.Process(prefix, &fromEnv)

	if err != nil {
		return nil, err
	}

	return &fromEnv, nil
}

// NewDefaultInfluxDBConfig is similar to NewInfluxDBConfigFromEnv
// with `prefix` set to INFLUXDB
func NewDefaultInfluxDBConfig() (*Config, error) {
	return NewInfluxDBConfigFromEnvironment(defaultPrefix)
}

// NewInfluxClient returns a new influxdb client based on the provided config
func NewInfluxClient(config *Config) influxdb2.Client {
	return influxdb2.NewClient(config.URL, config.Token)
}

// NewInfluxClientFromEnvironment returns a new influxdb client using the env variables
func NewInfluxClientFromEnvironment(prefix string) (influxdb2.Client, *Config) {
	config, err := NewInfluxDBConfigFromEnvironment(prefix)
	if err != nil {
		panic(err)
	}
	return NewInfluxClient(config), config
}

// NewDefaultInfluxClient is similar to NewInfluxClientFromEnvironment but always uses INFLUXDB as the prefix
func NewDefaultInfluxClient() (influxdb2.Client, *Config) {
	return NewInfluxClientFromEnvironment(defaultPrefix)
}
