module gitlab.com/tanqhnguyen/goinfluxdb

go 1.14

require (
	github.com/influxdata/influxdb-client-go v1.2.0
	github.com/tanqhnguyen/envconfig v1.5.4
)
